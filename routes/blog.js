const { Router } = require("express");
const multer = require("multer");
const path = require("path");

const Blog = require("../models/blog");
const Comment = require("../models/comment");

const router = Router();

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(`./public/uploads/`));
  },
  filename: function (req, file, cb) {
    const fileName = `${Date.now()}-${file.originalname}`;
    cb(null, fileName);
  },
});

const upload = multer({ storage });

router.route("/add-new").get((req, res) => {
  return res.render("addBlog", {
    user: req.user,
  });
});

router.route("/:blogId").get(async (req, res) => {
  const blog = await Blog.findById(req.params.blogId).populate("createdBy");
  const comments = await Comment.find({ blogId: req.params.blogId }).populate(
    "createdBy"
  );
  return res.render("blog", {
    user: req.user,
    blog,
    comments,
  });
});

router.route("/comment/:blogId").post(async (req, res) => {
  const { content } = req.body;
  const { blogId } = req.params;
  const { _id } = req.user;
  const coment = await Comment.create({
    content,
    blogId,
    createdBy: _id,
  });
  return res.redirect(`/blog/${blogId}`);
});

router.route("/").post(upload.single("coverImage"), async (req, res) => {
  const { title, body } = req.body;

  const blog = await Blog.create({
    title,
    body,
    createdBy: req.user._id,
    coverImageURL: `uploads/${req.file.filename}`,
  });

  return res.redirect(`/blog/${blog._id}`);
});

module.exports = router;
