const { Router } = require("express");
const User = require("../models/user");

const router = Router();

router.route("/signin").get((req, res) => {
  return res.render("signin");
});

router.route("/signup").get((req, res) => {
  return res.render("signup");
});

router.route("/signup").post(async (req, res) => {
  const { fullName, email, password } = req.body;
  try {
    await User.create({
      fullName,
      email,
      password,
    });

    return res.redirect("/");
  } catch (err) {
    console.log(err);
  }
});

router.route("/signin").post(async (req, res) => {
  const { email, password } = req.body;

  try {
    const token = await User.matchPasswordAndGenerateToken(email, password);

    return res.cookie("token", token).redirect("/");
  } catch (err) {
    return res.render("signin", {
      error: "Incorrect Email or Password",
    });
  }
});

router.route("/signout").get((req, res) => {
  res.clearCookie("token").redirect("/");
});

module.exports = router;
