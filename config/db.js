const mongoose = require("mongoose");

mongoose.set("strictQuery", true);

const connectToMongoDB = (url) => {
  mongoose
    .connect(url)
    .then(() => {
      console.log("Database Connected...!");
    })
    .catch((err) => console.log("Error while connecting -> ", err));
};

module.exports = connectToMongoDB;
